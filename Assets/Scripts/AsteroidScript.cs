﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour {

    public float asteroidHP = 0;
	// Use this for initialization
	void Start () {
        asteroidHP = GameManager.instance.gameSettings.asteroidHP;
        float asteroidHPMultiplier = (transform.localScale.x + transform.localScale.y + transform.localScale.z) / 3;
        asteroidHP *= asteroidHPMultiplier;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bullet")
        {
            asteroidHP -= GameManager.instance.gameSettings.bulletDmg;
            Destroy(other.gameObject);
        }
        if ((other.tag == "OuterEdges"))
        {
            asteroidHP = 0;
            Destroy(this.gameObject);
        }
        if(asteroidHP <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
