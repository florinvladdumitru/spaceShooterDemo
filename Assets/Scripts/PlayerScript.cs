﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{

    private Transform _transform;
    private Vector2 _inputAxis = Vector2.zero;
    private Vector3 _movement = Vector3.zero;
    private float _playerSpeed = 0;
    public float _playerHP = 0;
    public float _playerScore = 0;
    private float _bulletSpeed = 0;

    public GameObject bullet;
    public Text hpText;
    public Text scoreText;

    private void Awake()
    {
        _transform = transform;       
        _playerSpeed = GameManager.instance.gameSettings.playerSpeed;
        _playerHP = GameManager.instance.gameSettings.playerHP;
        _bulletSpeed = GameManager.instance.gameSettings.bulletSpeed;

    }
    void Start()
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("GameScene"));
        hpText.text = "HP " + _playerHP.ToString("F2");

    }


    void Update ()
    {
        Move();
        _movement = new Vector3(_inputAxis.x, 0, _inputAxis.y);
    }
    
    void FixedUpdate()
    {
        _transform.position += _movement * Time.deltaTime;
    }    

    void Move()
    {
        _inputAxis.x = 0;
        _inputAxis.y = 0;
        if (InputManager.instance.IsLeftPressed())
        {
            _inputAxis.x = -_playerSpeed;
        }
        if (InputManager.instance.IsRightPressed())
        {
            _inputAxis.x = _playerSpeed;
        }
        if (InputManager.instance.IsUpPressed())
        {
            _inputAxis.y = _playerSpeed;
        }
        if (InputManager.instance.IsDownPressed())
        {
            _inputAxis.y = -_playerSpeed;
        }
        if (InputManager.instance.IsFirePressed())
        {
            Fire();
        }
    }



    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Asteroid")
        {
            Destroy(other.gameObject);
            _playerHP -= other.GetComponent<AsteroidScript>().asteroidHP;
            _playerScore += other.GetComponent<AsteroidScript>().asteroidHP;
            hpText.text = "HP " + _playerHP.ToString("F2");
            scoreText.text = "SCORE " + _playerScore.ToString("F2");
        }
        if(_playerHP <= 0)
        {
            EndGame();
        }
    }

    void EndGame()
    {
        Destroy(gameObject);
        GameManager.instance.State = GameManager.GameState.END;
    }

    void Fire()
    {
        GameObject firedBullet = Instantiate(bullet, new Vector3(transform.position.x, transform.position.y, transform.position.z + 2), Quaternion.identity);
        //firedBullet.transform.parent = transform;
        firedBullet.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, _bulletSpeed), ForceMode.Impulse);
    }
}
