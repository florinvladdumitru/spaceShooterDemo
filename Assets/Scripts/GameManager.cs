﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    public GameSettings gameSettings;

    public enum GameState { NONE, MENU, GAME, END }

    private GameState _state = GameState.NONE;

   

    public GameState State
    {
        set
        {
            switch (value)
            {
                case GameState.MENU:                   
                    SceneManager.LoadScene("MenuScene", LoadSceneMode.Additive);
                    break;
                case GameState.GAME:
                    SceneManager.LoadScene("GameScene", LoadSceneMode.Additive);
                    if (_state == GameState.MENU)
                        SceneManager.UnloadSceneAsync("MenuScene"); 
                    if (_state == GameState.END)
                        SceneManager.UnloadSceneAsync("EndScene");
                    break;
                case GameState.END:
                    SceneManager.LoadScene("EndScene", LoadSceneMode.Additive);
                    if (_state == GameState.GAME)
                        SceneManager.UnloadSceneAsync("GameScene");
                   // SceneManager.SetActiveScene(SceneManager.GetSceneByName("EndScene"));

                    break;
            }
            _state = value;            
        }
        get { return _state; }
    }

    private void Awake()
    {       
        instance = this;
    }

    void Start()
    {
        State = GameState.MENU;
    }
}
