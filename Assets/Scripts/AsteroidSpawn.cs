﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawn : MonoBehaviour {

    public GameObject asteroid;

    private float _spawnRate = 0;
    private float timeSinceSpawn = 0;
    private float _spawnMultiplier = 0;
    private float _asteroidSpeed = 0;
    private double _timeTrack;
    private double _timeScale;



    void Start ()
    {
        _timeTrack = Time.time;
        _timeScale = GameManager.instance.gameSettings.timeScale;
        _spawnRate = GameManager.instance.gameSettings.asteroidSpawnRate;
        _spawnMultiplier = GameManager.instance.gameSettings.asteroidSpawnMultiplier;
        _asteroidSpeed = GameManager.instance.gameSettings.asteroidSpeed;        

        if (Random.Range(0, 1000) > 499)
        {
            _spawnRate += 1;
        }
    }
	
	void FixedUpdate ()
    {
        SpawnAsteroids();
    }

    void SpawnAsteroids()
    {
        timeSinceSpawn += Time.fixedDeltaTime;
        if (timeSinceSpawn >= _spawnRate)
        {
            if(Time.time - _timeTrack > _timeScale && _spawnRate > _spawnMultiplier)
            {
                _spawnRate -= _spawnMultiplier; // decrease the time between spawns with the value of spawn multiplier
                _timeTrack = Time.time;
            }

            Vector3 randomDirectionVector = RandomizeDirection(-10f, 10f, -66.6f, -10f);
            Vector3 randomScaleVector = RandomizeAsteroid();
            GameObject spawnedAsteroid = Instantiate(asteroid, transform.position, Quaternion.identity);
            spawnedAsteroid.transform.localScale = randomScaleVector;            
            spawnedAsteroid.transform.parent = this.transform;
            spawnedAsteroid.GetComponent<Rigidbody>().AddForce(randomDirectionVector, ForceMode.Impulse);
            spawnedAsteroid.GetComponent<Rigidbody>().AddTorque(RandomizeDirection(-1f, 1f, -1f, 1f), ForceMode.Impulse);            
            timeSinceSpawn = 0;
        }
    }

    Vector3 RandomizeDirection(float xMin, float xMax, float zMin, float zMax)
    {
        float x = Random.Range(xMin, xMax); // left or right direction
        float z = Random.Range(zMin, zMax); // actual speed af asteroid

        return new Vector3(x * _asteroidSpeed, 0, z * _asteroidSpeed);
    }

    Vector3 RandomizeAsteroid()
    {
        float size = Random.Range(1f, 2.5f);
        return new Vector3(size, size, size);
    }
}
