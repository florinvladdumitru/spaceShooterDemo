﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGeneration : MonoBehaviour {

    private float _mapSpeed = 0;
    private bool _generatedNewGround = false;
    private Vector3 _mapMovement = Vector3.zero;

	// Use this for initialization
	void Start ()
    {
        _mapSpeed = GameManager.instance.gameSettings.mapSpeed;
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        ScrollGround();

    }

    void ScrollGround()
    {
        if (transform.position.z <= 0 && !_generatedNewGround)
        {
            Instantiate(this, new Vector3(0, 0, transform.localScale.z * 10 - 1), Quaternion.identity);
            _generatedNewGround = true;
        }
        if (transform.position.z <= -transform.localScale.z * 10 - 1 && _generatedNewGround)
            Destroy(this.gameObject);

        _mapMovement = new Vector3(0, 0, _mapSpeed);
        transform.position -= _mapMovement * Time.deltaTime;
    }

    //NEED A GROUND MANAGER
}
