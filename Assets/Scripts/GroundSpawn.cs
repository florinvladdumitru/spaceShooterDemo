﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawn : MonoBehaviour {

    private float _mapSpeed = 0;
    private bool _generatedNewGround = false;
    private Vector3 _mapMovement = Vector3.zero;
    

    public GameObject ground;

    // Use this for initialization
    void Start()
    {
        _mapSpeed = GameManager.instance.gameSettings.mapSpeed;
        GameObject firstGround = Instantiate(ground, new Vector3(0, 0, 0), Quaternion.identity);
        firstGround.transform.parent = this.transform;
    }
    
	
	// Update is called once per frame
	void Update ()
    {
        ScrollGround();
    }

    void ScrollGround()
    {
        if (GetComponentsInChildren<Transform>()[1].position.z <= 0 && !_generatedNewGround)        
        {
            GameObject newGround = Instantiate(ground, new Vector3(0, 0, GetComponentsInChildren<Transform>()[1].localScale.z * 10), Quaternion.identity);
            newGround.transform.parent = this.transform;
            _generatedNewGround = true;
        }
        if (GetComponentsInChildren<Transform>()[1].transform.position.z <= -GetComponentsInChildren<Transform>()[1].localScale.z * 10)
        {
            Destroy(GetComponentsInChildren<Transform>()[1].transform.gameObject);
            _generatedNewGround = false;
        }

        _mapMovement = new Vector3(0, 0, _mapSpeed);

        if(GetComponentsInChildren<Transform>()[1].gameObject != null)
            GetComponentsInChildren<Transform>()[1].transform.position -= _mapMovement * Time.deltaTime;
        if (GetComponentsInChildren<Transform>().Length > 2)
            GetComponentsInChildren<Transform>()[2].transform.position -= _mapMovement * Time.deltaTime;
    }
}
