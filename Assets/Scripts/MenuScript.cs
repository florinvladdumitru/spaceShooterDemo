﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    
    public void Play()
    {
        GameManager.instance.State = GameManager.GameState.GAME;        
    }

    public void Quit()
    {
        if (Application.isPlaying)
            Application.Quit();
    }
}
