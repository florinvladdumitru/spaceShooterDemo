﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public static InputManager instance; 


    void Awake()
    {
        instance = this;
    }  

    #region Keys & Axis Inputs

    public bool IsRightPressed()
    {       
        if (Input.GetAxis("Horizontal") > 0 || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            return true;
        }        
        return false;
    }

    public bool IsLeftPressed()
    {
        if (Input.GetAxis("Horizontal") < 0 || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            return true;
        }
        return false;
    }
    public bool IsUpPressed()
    {
        if (Input.GetAxis("Vertical") > 0 || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            return true;
        }
        return false;
    }
    public bool IsDownPressed()
    {
        if (Input.GetAxis("Vertical") < 0 || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            return true;
        }
        return false;
    }

    public bool IsFirePressed()
    {
        if (Input.GetButtonDown("Fire1") || Input.GetKeyDown(KeyCode.Space) 
            || Input.GetButtonDown("Fire2") || Input.GetKeyDown(KeyCode.LeftControl))
        {
            return true;
        }
        return false;
    }

    public Vector3 Move(float x, float y)
    {
        return new Vector3(x, 0, y);
    }

    #endregion
}
