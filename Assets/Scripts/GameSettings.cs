﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpaceShooter/GameSettings")]
public class GameSettings : ScriptableObject
{

    [Header("Player")]
    public float playerSpeed;   
    public float playerHP;
    public float playerDmg;
    [Space]
    public float bulletDmg;
    public float bulletSpeed;

    [Header("Asteroids")]
    public float asteroidSpeed;    
    public float asteroidHP;
    public float asteroidSpawnRate;
    public float asteroidSpawnMultiplier;

    [Header("Map")]
    public float mapSpeed;   
    public float mapLength;
    public float timeScale;


}
